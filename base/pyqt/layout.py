# _*_ coding: UTF-8 _*_
'''
Created on 08/03/2019

@author: osni
'''
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout, QWidget, QTableWidget, QAbstractItemView, QFormLayout, \
    QMainWindow, qApp, QAction

from base.pyqt.widget import Label
from config.settings import icon_path


class LayoutVertical(QVBoxLayout):
    def __init__(self):
        super(LayoutVertical, self).__init__()


class LayoutHorizontal(QHBoxLayout):
    def __init__(self):
        super(LayoutHorizontal, self).__init__()


class Widget(QWidget):
    def __init__(self, parent=None):
        super(Widget, self).__init__(parent)
        self.setWindowIcon(QIcon(icon_path + 'logo_icon.ico'))


class MainWindow(QMainWindow):
    def client_form(self):
        pass

    def bill_form(self):
        pass

    def item_form(self):
        pass

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowIcon(QIcon(icon_path + 'logo_icon.ico'))
        # Menu bar
        self.menu = self.menuBar()
        font = self.menu.font()
        font.setPointSize(15)
        self.menu.setFont(font)
        # Exit Action
        exitAction = QAction(QIcon(icon_path + 'exit.png'), "&Sair", self)
        exitAction.setShortcut("Ctrl+Q")
        exitAction.setStatusTip('Sair do programa')
        exitAction.triggered.connect(qApp.quit)
        # Clients Action
        clientAction = QAction(QIcon(icon_path + 'svg\\016-manager.svg'), "&Clientes", self)
        clientAction.setShortcut("F3")
        clientAction.setStatusTip('Gerenciamento de clientes')
        clientAction.triggered.connect(self.client_form)
        # Faturas action
        billAction = QAction(QIcon(icon_path + 'svg\\051-shopping-cart.svg'), "&Faturas", self)
        billAction.setShortcut("F4")
        billAction.setStatusTip('Criar e pesquisar faturas')
        billAction.triggered.connect(self.bill_form)
        # Itens action
        itemAction = QAction(QIcon(icon_path + 'svg\\013-shopping-basket.svg'), "&Produtos", self)
        itemAction.setShortcut("F5")
        itemAction.setStatusTip('Gerenciamento de produtos')
        itemAction.triggered.connect(self.item_form)
        # Add Menus
        # self.menu.addMenu('&Importar')
        # fileMenu = self.menu.addMenu('&Arquivo')
        # fileMenu.addAction(exitAction)
        # toolbar
        self.toolbar = self.addToolBar('')
        self.toolbar.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolbar.addAction(clientAction)
        self.toolbar.addAction(billAction)
        self.toolbar.addAction(itemAction)
        self.toolbar.addAction(exitAction)

        self.status = self.statusBar()
        # self.window.setStatusBar(self.statusbar)

        self.status_message_center = Label(self, 'Clientes em atraso : ')
        self.status_message_right = Label(self, 'Faturas vencendo : ')
        self.status_message_left = Label(self, 'Licenciado para : DEV')

        self.status.addWidget(self.status_message_left, 1)
        self.status.addWidget(self.status_message_center, 2)
        self.status.addWidget(self.status_message_right, 0)



class FormLayout(QFormLayout):

    def __init__(self):
        super(FormLayout, self).__init__()


class Grid(QTableWidget):

    def __init__(self, parent=None, qtde_linhas=0, qtde_colunas=0):
        super(Grid, self).__init__(parent)

        self.setRowCount(qtde_linhas)
        self.setColumnCount(qtde_colunas)

        # redimenciona a linha automaticamente
        self.resizeRowsToContents()

        # coloca as linhas em cores alternadas
        self.setAlternatingRowColors(True)

        # habilita a ordenação ao clicar no titulo da coluna
        self.setSortingEnabled(True)

        # seleciona toda a linha quando clicado
        self.setSelectionBehavior(QAbstractItemView.SelectRows)
