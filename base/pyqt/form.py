# _*_ coding: UTF-8 _*_
'''
Created on 08/03/2019

@author: osni
'''

from base.pyqt.layout import *
from base.pyqt.widget import *


class FrmCadDefault(Dialog):

    def __init__(self, parent=None):
        super(FrmCadDefault, self).__init__(parent)
        self.model = None
        self.FrmCadDefaultCreate()
    def new(self):
        pass

    def save(self):
        pass

    def search(self):
        pass

    def delete(self):
        pass

    def FrmCadDefaultCreate(self):
        # =======================================================================
        # CUSTOMIZANDO A JANETA
        # =======================================================================
        self.setWindowTitle(u'Cadastro padrão')

        self.layoutPrincipal = LayoutVertical()

        self.txtCodigo = TextBox(self)
        self.txtCodigo.setReadOnly(True)
        self.txtCodigo.setFixedWidth(170)

        self.ckAtivo = CheckBox(self, text='Ativo')
        self.ckAtivo.setChecked(True)

        hboxcodigo = LayoutHorizontal()
        hboxcodigo.addWidget(self.txtCodigo)
        hboxcodigo.addWidget(self.ckAtivo)
        hboxcodigo.addStretch(2)

        self.fLayout = FormLayout()
        self.fLayout.addRow(u'Código', hboxcodigo)

        # adicionando o form layout ao layout rincipal
        self.layoutPrincipal.addLayout(self.fLayout)

        # =======================================================================
        # CRIANDO OS BOTOES
        # =======================================================================
        self.btnNovo = Button(self, text='&Novo')
        self.btnSalvar = Button(self, text='&Salvar')
        self.btnExcluir = Button(self, text='&Excluir')
        self.btnPesquisar = Button(self, text='&Pesquisar')
        self.btnNovo.clicked.connect(self.new)
        self.btnSalvar.clicked.connect(self.save)
        self.btnExcluir.clicked.connect(self.delete)
        self.btnPesquisar.clicked.connect(self.search)

        hboxBotoes = LayoutHorizontal()
        hboxBotoes.addStretch(2)
        hboxBotoes.addWidget(self.btnNovo)
        hboxBotoes.addWidget(self.btnSalvar)
        hboxBotoes.addWidget(self.btnExcluir)
        hboxBotoes.addWidget(self.btnPesquisar)
        hboxBotoes.setSpacing(1)

        # adicionando os botoes ao layout principal
        self.layoutPrincipal.addLayout(hboxBotoes)

        self.setLayout(self.layoutPrincipal)


if __name__ == '__main__':
    import sys

    root = QApplication(sys.argv)
    app = FrmCadDefault(None)
    app.show()
    root.exec_()
