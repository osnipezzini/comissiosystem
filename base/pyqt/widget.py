# _*_ coding: UTF-8 _*_
'''
Created on 08/03/2019

@author: osni
'''
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import *

from config.settings import *


class TextBox(QLineEdit):
    def __init__(self, parent=None):
        super(TextBox, self).__init__(parent)


class Label(QLabel):
    def __init__(self, parent=None, text=''):
        super(Label, self).__init__(parent)
        self.setText(text)


class CheckBox(QCheckBox):
    def __init__(self, parent=None, text=''):
        super(CheckBox, self).__init__(parent)
        self.setText(text)


class Button(QPushButton):
    def __init__(self, parent=None, text=''):
        super(Button, self).__init__(parent)
        self.setText(text)


class MessageBox(QMessageBox):

    def __init__(self, parent=None):
        super(MessageBox, self).__init__(parent)


class Dialog(QDialog):

    def __init__(self, parent=None):
        super(Dialog, self).__init__(parent)


class TableItem(QTableWidgetItem):
    def __init__(self, parent=None):
        super(TableItem, self).__init__(parent)


class Table(QTableWidget):
    def __init__(self, parent=None, qtde_linhas=0, qtde_colunas=0):
        super(Table, self).__init__(parent)

        self.setRowCount(qtde_linhas)
        self.setColumnCount(qtde_colunas)

        # redimenciona a linha automaticamente
        self.resizeRowsToContents()

        # coloca as linhas em cores alternadas
        self.setAlternatingRowColors(True)

        # habilita a ordenação ao clicar no titulo da coluna
        self.setSortingEnabled(True)

        # seleciona toda a linha quando clicado
        self.setSelectionBehavior(QAbstractItemView.SelectRows)


class MenuBar(QMenuBar):
    def __init__(self, parent=None):
        super(MenuBar, self).__init__(parent)
        font = self.font()
        font.setPointSize(15)
        self.setFont(font)




class ComboBox(QComboBox):
    def __init__(self, parent=None):
        super(ComboBox, self).__init__(parent)

class StatusBar(QStatusBar):
    def __init__(self, parent=None):
        super(StatusBar, self).__init__(parent)