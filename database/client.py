import peewee

from base.client import BaseModel


class Client(BaseModel):
    """
    Classe que representa a tabela Author
    """

    # A tabela possui apenas o campo 'name', que
    # receberá o nome do autor
    lzt_id = peewee.IntegerField()
    name = peewee.CharField()
    cnpj = peewee.CharField(unique=True)
    cidade = peewee.CharField()
    uf = peewee.CharField()
    key_days = peewee.IntegerField()
    password = peewee.CharField()
    # class Meta:
    #     indexes = (
    #         (("id", "cnpj"), True),
    #     )

