from datetime import date, datetime

import peewee

from base.client import BaseModel
from database.client import Client


class Item(BaseModel):
    """
    Classe que representa a tabela Author
    """

    # A tabela possui apenas o campo 'name', que
    # receberá o nome do autor
    item = peewee.CharField()
    added = peewee.DateTimeField(default=datetime.now())


