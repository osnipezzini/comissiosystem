from datetime import date, datetime

import peewee

from base.client import BaseModel
from database.client import Client
from database.item import Item


class Comission(BaseModel):
    """
    Classe que representa a tabela Author
    """

    # A tabela possui apenas o campo 'name', que
    # receberá o nome do autor
    client = peewee.ForeignKeyField(Client, backref='comissions')
    fatura = peewee.CharField()
    emissao = peewee.DateField()
    vencto = peewee.DateField()
    pagto = peewee.DateField()
    item = peewee.ForeignKeyField(Item, backref='items')
    valor_orig = peewee.DecimalField(decimal_places=3)
    valor_baixado = peewee.DecimalField(decimal_places=3)
    comissao_percent = peewee.DecimalField()
    comissao_valor = peewee.DecimalField()
    imposto_percent = peewee.DecimalField()
    imposto_valor = peewee.DecimalField()
    comissao_pagar = peewee.DecimalField()
    added = peewee.DateTimeField(default=datetime.now())
