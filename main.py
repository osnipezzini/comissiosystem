from config.settings import icon_path
from database.client import Client
from gui.pyqt.forms.bill import BillForm
from gui.pyqt.forms.client import ClientForm
from gui.pyqt.forms.item import ItemForm

client = Client()

from PyQt5.QtWidgets import QApplication, QDesktopWidget

from base.pyqt.layout import Widget, MainWindow
from database.comission import Comission

comission = Comission()
count = comission.select().where(Comission.vencto < Comission.pagto).count()
comissoes = comission.select().execute()


class MainGui(MainWindow):

    def client_form(self):
        frmClient = ClientForm(self)
        # frmPesquisa.setModal(False)
        frmClient.show()

    def bill_form(self):
        frmClient = BillForm(self)
        # frmPesquisa.setModal(False)
        frmClient.show()

    def item_form(self):
        frmClient = ItemForm(self)
        # frmPesquisa.setModal(False)
        frmClient.show()


    def __init__(self, parent=None):
        super(MainGui, self).__init__(parent)

        self.main_widget = MainWidget(self)

        self.setCentralWidget(self.main_widget)

        self.init_UI()
        # self.status = self.statusBar()
        # self.status.showMessage('Ready')
        self.showMaximized()

    def init_UI(self):
        # self.statusBar().showMessage(f'Ready')
        self.setGeometry(200, 100, 300, 300)
        self.setWindowTitle('Sistema de controle de faturas')
        self.show()


class MainWidget(Widget):

    def __init__(self, parent=None):
        super(MainWidget, self).__init__(parent)
        self.parent = parent
        self.setStyleSheet("QWidget {background-image: url(" + icon_path + "company-bg.jpg)}")

    def CenterOnScreen(self):
        '''alinhando a o formulario no centro da tela'''
        resolucao = QDesktopWidget().screenGeometry()
        self.move((resolucao.width() / 2) - (self.frameSize().width() / 2),
                  (resolucao.height() / 2) - (self.frameSize().height() / 2))


if __name__ == '__main__':
    import sys

    root = QApplication(sys.argv)
    app = MainGui()
    app.show()
    root.exec_()
