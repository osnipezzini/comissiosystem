import os
from pathlib import Path  # python3 only

basepath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
print(basepath)
icon_path = basepath + '/share/icons/'
from dotenv import load_dotenv

env_path = Path(basepath) / '.env'
load_dotenv(dotenv_path=env_path)

HOST = os.getenv("PGHOST")
USER = os.getenv("PGUSER")
PASS = os.getenv("PGPASSWORD")
PORT = os.getenv("PGPORT")
DATABASE = os.getenv("PGDATABASE")


def getconn():
    db = f"postgresql://{USER}:{PASS}@{HOST}:{PORT}/{DATABASE}"
    return db
