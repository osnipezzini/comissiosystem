# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.grid

###########################################################################
## Class MainGui
###########################################################################

class MainGui ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"ComissionSystem", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )
		self.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )

		self.m_menubar1 = wx.MenuBar( 0 )
		self.file_menu = wx.Menu()
		self.import_csv_file = wx.MenuItem( self.file_menu, wx.ID_ANY, u"Importar Comissões", wx.EmptyString, wx.ITEM_NORMAL )
		self.file_menu.Append( self.import_csv_file )

		self.import_client_table = wx.MenuItem( self.file_menu, wx.ID_ANY, u"Importar Clientes", wx.EmptyString, wx.ITEM_NORMAL )
		self.file_menu.Append( self.import_client_table )

		self.m_menubar1.Append( self.file_menu, u"Arquivo" )

		self.opes = wx.Menu()
		self.m_menubar1.Append( self.opes, u"Opções" )

		self.SetMenuBar( self.m_menubar1 )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText1 = wx.StaticText( self, wx.ID_ANY, u"Relação de Clientes", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_staticText1.Wrap( -1 )

		bSizer1.Add( self.m_staticText1, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )

		self.grid_client = wx.grid.Grid( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )

		# Grid
		self.grid_client.CreateGrid( 1, 1 )
		self.grid_client.EnableEditing( True )
		self.grid_client.EnableGridLines( True )
		self.grid_client.EnableDragGridSize( False )
		self.grid_client.SetMargins( 0, 0 )

		# Columns
		self.grid_client.AutoSizeColumns()
		self.grid_client.EnableDragColMove( True )
		self.grid_client.EnableDragColSize( True )
		self.grid_client.SetColLabelSize( 30 )
		self.grid_client.SetColLabelAlignment( wx.ALIGN_CENTER, wx.ALIGN_CENTER )

		# Rows
		self.grid_client.AutoSizeRows()
		self.grid_client.EnableDragRowSize( True )
		self.grid_client.SetRowLabelSize( 80 )
		self.grid_client.SetRowLabelAlignment( wx.ALIGN_CENTER, wx.ALIGN_CENTER )

		# Label Appearance

		# Cell Defaults
		self.grid_client.SetDefaultCellAlignment( wx.ALIGN_LEFT, wx.ALIGN_TOP )
		bSizer1.Add( self.grid_client, 0, wx.ALL, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_MENU, self.import_csv_file, id = self.import_csv_file.GetId() )
		self.Bind( wx.EVT_MENU, self.import_clients, id = self.import_client_table.GetId() )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def import_csv_file( self, event ):
		event.Skip()

	def import_clients( self, event ):
		event.Skip()


