# _*_ coding: UTF-8 _*_

'''
Created on 08/03/2019
@author: osnip
'''
from PyQt5.QtCore import pyqtSlot

from base.pyqt.form import FrmCadDefault, TextBox, Dialog, LayoutVertical, Button, LayoutHorizontal, Table, \
    QAbstractItemView, TableItem, QDesktopWidget
from base.pyqt.widget import Label
from database.item import Item

itemModel = Item


class ItemSearch(Dialog):

    @pyqtSlot()
    def on_click(self):
        item_form = ItemForm(codigo=self.grdPesquisaCliente.item(self.grdPesquisaCliente.currentRow(), 0).text())
        item_form.show()
        ItemSearch.close()

    def __init__(self, parent=None):
        super(ItemSearch, self).__init__(parent)

        self.FrmPesquisaClienteCreate()

        # conectando o click dos botoes
        self.btnBuscar.clicked.connect(self.search)
        self.grdPesquisaCliente.doubleClicked.connect(self.on_click)
        # criando um objeto cliente DB ara que fique a disposição para interagir com o módulo de acesso a dados
        self.CenterOnScreen()

    def FrmPesquisaClienteCreate(self):
        # =======================================================================
        # customizando a tela de pesquisa
        # =======================================================================
        self.setWindowTitle('Pesquisa de cliente')
        self.resize(800, 300)

        self.layoutPrincipal = LayoutVertical()

        # criando o campo de busca
        self.lblBusca = Label(self, 'Digite a busca')
        self.txtBusca = TextBox(self)
        self.btnBuscar = Button(self, '&Buscar')
        # self.txtBusca.textChanged.connect(self.search)

        hboxBusca = LayoutHorizontal()
        hboxBusca.addWidget(self.lblBusca)
        hboxBusca.addWidget(self.txtBusca)
        hboxBusca.addWidget(self.btnBuscar)

        self.layoutPrincipal.addLayout(hboxBusca)

        # criando o grid da pesquisa
        cabecalh_grid = [u'Código', 'Item']
        self.grdPesquisaCliente = Table(self, 0, len(cabecalh_grid))
        self.grdPesquisaCliente.setHorizontalHeaderLabels(cabecalh_grid)

        # desativando edição do grid de pesquisa
        self.grdPesquisaCliente.setEditTriggers(QAbstractItemView.NoEditTriggers)

        # recidimencionando as colunas do grid
        self.grdPesquisaCliente.setColumnWidth(1, 300)
        self.grdPesquisaCliente.setColumnWidth(2, 200)

        self.layoutPrincipal.addWidget(self.grdPesquisaCliente)

        self.setLayout(self.layoutPrincipal)

    def search(self, e):
        '''Método faz uma busca de clientes no banco de dados, e cria uma lista de tuplas'''
        # faz a consulta no banco e recebe uma lista de tuplas
        texto_busca = str(self.txtBusca.text())
        item_search = Item.select().where(Item.item.contains(texto_busca))
        items = item_search.execute()

        # setando no grid a qtde de linhas, com a mesma qtde de registros da lista
        qtde_registros = len(items)
        self.grdPesquisaCliente.setRowCount(qtde_registros)

        linha = 0
        for item in items:
            # preencendo o grid de pesquisa
            self.grdPesquisaCliente.setItem(linha, 0, TableItem(str(item.id)))
            self.grdPesquisaCliente.setItem(linha, 1, TableItem(item.item))

            linha += 1

    def CenterOnScreen(self):
        '''alinhando a o formulario no centro da tela'''
        resolucao = QDesktopWidget().screenGeometry()
        self.move((resolucao.width() / 2) - (self.frameSize().width() / 2),
                  (resolucao.height() / 2) - (self.frameSize().height() / 2))


class ItemForm(FrmCadDefault):
    def __init__(self, parent=None, codigo=0):
        super(ItemForm, self).__init__(parent)
        self.in_item = TextBox(self)
        self.fLayout.addRow('Item', self.in_item)
        self.parent = parent

        if not codigo == 0:
            item = Item.get(id=int(codigo))
            self.in_item.setText(item.item)
            self.txtCodigo.setText(str(item.id))
        # Inputs

        self.setWindowTitle(u'Cadastro de produtos')

    def save(self):
        pass

    def search(self):
        item_search = ItemSearch(self)
        item_search.show()
        self.close()

    def delete(self):
        pass
