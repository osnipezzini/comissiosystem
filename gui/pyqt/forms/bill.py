# _*_ coding: UTF-8 _*_

'''
Created on 08/03/2019
@author: osnip
'''
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAbstractItemView

from base.pyqt.form import FrmCadDefault, TextBox, QDesktopWidget, TableItem, Table, LayoutHorizontal, Button, \
    LayoutVertical, Dialog
from base.pyqt.widget import Label
from config.settings import icon_path
from database.comission import Comission


class BillSearch(Dialog):

    @pyqtSlot()
    def on_click(self):
        item_form = BillForm(codigo=self.grdPesquisaCliente.item(self.grdPesquisaCliente.currentRow(), 0).text())
        item_form.show()


    def __init__(self, parent=None):
        super(BillSearch, self).__init__(parent)

        self.FrmPesquisaClienteCreate()

        # conectando o click dos botoes
        self.btnBuscar.clicked.connect(self.search)
        self.grdPesquisaCliente.doubleClicked.connect(self.on_click)
        # criando um objeto cliente DB ara que fique a disposição para interagir com o módulo de acesso a dados
        self.CenterOnScreen()

    def FrmPesquisaClienteCreate(self):
        # =======================================================================
        # customizando a tela de pesquisa
        # =======================================================================
        self.setWindowTitle('Pesquisa de fatura')
        self.setWindowIcon(QIcon(icon_path + 'logo_icon.ico'))
        self.resize(800, 300)

        self.layoutPrincipal = LayoutVertical()

        # criando o campo de busca
        self.lblBusca = Label(self, 'Digite a busca')
        self.txtBusca = TextBox(self)
        self.btnBuscar = Button(self, '&Buscar')
        # self.txtBusca.textChanged.connect(self.search)

        hboxBusca = LayoutHorizontal()
        hboxBusca.addWidget(self.lblBusca)
        hboxBusca.addWidget(self.txtBusca)
        hboxBusca.addWidget(self.btnBuscar)

        self.layoutPrincipal.addLayout(hboxBusca)

        # criando o grid da pesquisa
        cabecalh_grid = [u'Código', 'Item']
        self.grdPesquisaCliente = Table(self, 0, len(cabecalh_grid))
        self.grdPesquisaCliente.setHorizontalHeaderLabels(cabecalh_grid)

        # desativando edição do grid de pesquisa
        self.grdPesquisaCliente.setEditTriggers(QAbstractItemView.NoEditTriggers)

        # recidimencionando as colunas do grid
        self.grdPesquisaCliente.setColumnWidth(1, 300)
        self.grdPesquisaCliente.setColumnWidth(2, 200)

        self.layoutPrincipal.addWidget(self.grdPesquisaCliente)

        self.setLayout(self.layoutPrincipal)

    def search(self, e):
        '''Método faz uma busca de clientes no banco de dados, e cria uma lista de tuplas'''
        # faz a consulta no banco e recebe uma lista de tuplas
        texto_busca = str(self.txtBusca.text())
        item_search = Comission.select().where(Comission.fatura.contains(texto_busca))
        items = item_search.execute()

        # setando no grid a qtde de linhas, com a mesma qtde de registros da lista
        qtde_registros = len(items)
        self.grdPesquisaCliente.setRowCount(qtde_registros)

        linha = 0
        for item in items:
            # preencendo o grid de pesquisa
            self.grdPesquisaCliente.setItem(linha, 0, TableItem(str(item.id)))
            self.grdPesquisaCliente.setItem(linha, 1, TableItem(item.fatura))

            linha += 1

    def CenterOnScreen(self):
        '''alinhando a o formulario no centro da tela'''
        resolucao = QDesktopWidget().screenGeometry()
        self.move((resolucao.width() / 2) - (self.frameSize().width() / 2),
                  (resolucao.height() / 2) - (self.frameSize().height() / 2))


class BillForm(FrmCadDefault):
    def __init__(self, parent=None, codigo=0):
        super(BillForm, self).__init__(parent)

        # Inputs
        self.in_cliente = TextBox(self)
        self.in_bill = TextBox(self)
        self.in_emissao = TextBox(self)
        self.in_vencto = TextBox(self)
        self.in_pagto = TextBox(self)
        self.in_item = TextBox(self)
        self.in_valor_orig = TextBox(self)
        self.in_valor_baixado = TextBox(self)
        self.in_comissao_percent = TextBox(self)
        self.in_comissao_valor = TextBox(self)
        self.in_imposto_percent = TextBox(self)
        self.in_imposto_valor = TextBox(self)
        self.in_comissao_pagar = TextBox(self)
        self.fLayout.addRow('Cliente', self.in_cliente)
        self.fLayout.addRow('Fatura', self.in_bill)
        self.fLayout.addRow('Data de emissão', self.in_emissao)
        self.fLayout.addRow('Data de vencimento', self.in_vencto)
        self.fLayout.addRow('Data de Pagamento', self.in_pagto)
        self.fLayout.addRow('Produto', self.in_item)
        self.fLayout.addRow('Valor original', self.in_valor_orig)
        self.fLayout.addRow('Valor baixado', self.in_valor_baixado)
        self.fLayout.addRow('Comissão %', self.in_comissao_percent)
        self.fLayout.addRow('Comissão valor', self.in_comissao_valor)
        self.fLayout.addRow('Imposto %', self.in_imposto_percent)
        self.fLayout.addRow('Imposto valor', self.in_imposto_valor)
        self.fLayout.addRow('Comissão a pagar', self.in_comissao_pagar)
        self.setWindowTitle(u'Cadastro de Faturas')

        if not codigo == 0:
            item = Comission.get(id=int(codigo))
            self.in_bill.setText(item.fatura)
            self.txtCodigo.setText(str(item.id))

    def save(self):
        pass

    def search(self):
        item_search = BillSearch(self)
        item_search.show()
        self.close()

    def delete(self):
        pass
