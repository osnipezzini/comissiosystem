# _*_ coding: UTF-8 _*_

'''
Created on 08/03/2019
@author: osnip
'''
from base.pyqt.form import FrmCadDefault, TextBox
from base.pyqt.widget import Label


class ClientForm(FrmCadDefault):
    def __init__(self, parent=None):
        super(ClientForm, self).__init__(parent)

        # Inputs
        self.in_cnpj = TextBox(self)
        self.in_name = TextBox(self)
        self.in_city = TextBox(self)
        self.in_uf = TextBox(self)
        self.fLayout.addRow('CNPJ', self.in_cnpj)
        self.fLayout.addRow('Nome Fantasia', self.in_name)
        self.fLayout.addRow('Cidade', self.in_city)
        self.fLayout.addRow('Estado', self.in_uf)
        self.setWindowTitle(u'Cadastro de clientes')

    def save(self):
        pass

    def search(self):
        pass

    def delete(self):
        pass
