import sys

from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QAction, qApp

from base.pyqt.layout import Widget, LayoutVertical, MainWindow
from base.pyqt.widget import Table, MenuBar, TableItem
from classes.csv_import import WindowCommission, WindowComissionMain
from config.settings import icon_path
from database.client import Client

client = Client()


class MyMainWindow(MainWindow):

    def __init__(self, parent=None):
        super(MyMainWindow, self).__init__()
        self.main_widget = App()
        self.setCentralWidget(self.main_widget)
        self.init_UI()


    def init_UI(self):
        self.statusBar().showMessage(f'Ready')
        self.setGeometry(200, 100, 300, 300)
        self.setWindowTitle(u'Sistema de comissões Linx')
        self.show()


class App(Widget):

    def __init__(self):
        super().__init__()
        self.left = 0
        self.top = 0
        self.width = 1366
        self.height = 768
        self.initUI()
        self.setWindowTitle(u'Sistema de comissões Linx')

    def open_comission(self):
        frmPesquisa = WindowComissionMain(self)
        # frmPesquisa.setModal(False)
        frmPesquisa.show()

    def initUI(self):

        self.createTable()
        self.menu = MenuBar()
        extractAction = QAction(QIcon(icon_path + 'exit.png'), "&Exit", self)
        extractAction.setShortcut("Ctrl+Q")
        extractAction.setStatusTip('Sair do programa')
        extractAction.triggered.connect(qApp.quit)
        fileMenu = self.menu.addMenu('&Arquivo')
        fileMenu.addAction(extractAction)

        import_comissao = QAction(u"&Importar Comissões", self)
        import_comissao.triggered.connect(self.open_comission)
        importmenu = self.menu.addMenu('&Importar')
        importmenu.addAction(import_comissao)
        # Add box layout, add table to box layout and add box layout to widget
        self.layout = LayoutVertical()
        self.layout.addWidget(self.menu)

        self.layout.addWidget(self.tableWidget)
        self.setLayout(self.layout)

        # Show widget
        self.show()

    def createTable(self):
        # Create table
        count = client.select().count()
        row = 0
        self.tableWidget = Table(None, count, 5)
        self.tableWidget.setHorizontalHeaderLabels(('LZT ID', 'Nome Fantasia', 'CNPJ', 'Cidade', 'Estado'))
        clientes = client.select().execute()
        for cliente in clientes:
            self.tableWidget.setItem(row, 0, TableItem(str(cliente.lzt_id)))
            self.tableWidget.setItem(row, 1, TableItem(cliente.name))
            self.tableWidget.setItem(row, 2, TableItem(cliente.cnpj))
            self.tableWidget.setItem(row, 3, TableItem(cliente.cidade))
            self.tableWidget.setItem(row, 4, TableItem(cliente.uf))
            self.tableWidget.move(0, 0)
            row += 1
        self.tableWidget.resizeColumnsToContents()
        self.showMaximized()

        # table selection change
        self.tableWidget.doubleClicked.connect(self.on_click)

    @pyqtSlot()
    def on_click(self):
        print("\n")
        for currentQTableWidgetItem in self.tableWidget.selectedItems():
            print(currentQTableWidgetItem.row(), currentQTableWidgetItem.column(), currentQTableWidgetItem.text())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
