import csv
import os
import re
from config import settings
from database.client import Client
from database.comission import Comission
from database.item import Item

path = settings.basepath + '\\share\\csv\\'

class Comissao:
    def save_to_db(self):
        with open(path + 'linx_comissao_padrao_novo.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=';')
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    line_count += 1
                else:
                    teste = row[1]
                    if len(teste) < 14:
                        teste = teste.zfill(14)
                    cnpj = '{}.{}.{}/{}-{}'.format(teste[:2], teste[2:5], teste[5:8], teste[8:12],teste[12:])
                    print(cnpj)  # 123.456.789-00
                    client = Client.get_or_none(cnpj=cnpj)
                    item = Item.get_or_create(item=row[6])
                    item_id = Comission.get_or_none(item_id=item[0])
                    fatura = Comission.get_or_none(fatura=row[2])
                    if Comission.get_or_none(item_id=item[0]) is None and Comission.get_or_none(fatura=row[2]) is None:
                        Comission.create(client=client,fatura=row[2],emissao=row[3],vencto=row[4],pagto=row[5],
                                         item_id=item[0],valor_orig=row[7],valor_baixado=row[8], comissao_percent=row[9],
                                         comissao_valor=row[10],imposto_percent=row[11], imposto_valor=row[12],
                                         comissao_pagar=row[13])

    def import_file(self):
        with open(path + 'linx_comissao_padrao.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=';')
            line_count = 0
            if os.path.exists(path + 'linx_comissao_padrao_novo.csv'):
                os.remove(path + 'linx_comissao_padrao_novo.csv')
            for row in csv_reader:
                if line_count == 0:
                    print(f'Column names are {", ".join(row)}')
                    line_count += 1
                    with open(path + 'linx_comissao_padrao_novo.csv', "a") as file:
                        column_names = "cliente;cnpj;fatura;emissao;vencto;pgto;item;valor_orig;valor_baixado;comissao_percent;comissao_valor;impostos_percent;imposto_valor;comissao_pagar\n"
                        file.write(column_names)
                    file.close()
                else:
                    clifor = int(re.search(r'\d+', row[0]).group())
                    valor = re.findall(r"[-+]?\d*\.\d+|\d+", row[6])
                    valor_orig = valor[0] + valor[1]
                    ab = float(valor_orig)
                    cliente = " ".join(re.findall("[a-zA-Z]+", row[0]))
                    cnpj = str(row[1])
                    comissao = row[7].split()
                    comissao_percent = comissao[0]
                    comissao_valor = comissao[1]
                    taxa_percent = comissao[2]
                    taxa_valor = comissao[3]
                    item = " ".join(re.findall("[a-zA-Z]+", row[6]))
                    line_count += 1
                    with open(path + 'linx_comissao_padrao_novo.csv', "a") as file:
                        string = f"{cliente};{cnpj};{row[2]};{row[3]};{row[4]};{row[5]};{item};" \
                            f"{valor_orig};{valor[2]};{comissao_percent};{comissao_valor};{taxa_percent};{taxa_valor};{row[8]}\n"
                        file.write(string)
                    file.close()
            print(f'Processed {line_count} lines.')

