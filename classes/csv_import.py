from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QDesktopWidget, QAction, qApp

from base.pyqt.layout import Widget, LayoutVertical, LayoutHorizontal, MainWindow
from base.pyqt.widget import TableItem, Table, Label, TextBox, Button, ComboBox, MenuBar
from database.comission import Comission

comission = Comission()
count = comission.select().where(Comission.vencto < Comission.pagto).count()
comissoes = comission.select().execute()


class WindowComissionMain(MainWindow):

    def __init__(self, parent=None):
        super(WindowComissionMain, self).__init__(parent)
        self.main_widget = WindowCommission(self)


        self.setCentralWidget(self.main_widget)
        self.init_UI()
        # self.status = self.statusBar()
        # self.status.showMessage('Ready')
        self.showMaximized()

    def init_UI(self):
        # self.statusBar().showMessage(f'Ready')
        self.setGeometry(200, 100, 300, 300)
        self.setWindowTitle('Clientes em atraso')
        self.show()


class WindowCommission(Widget):

    def setColortoRow(table, rowIndex, color):
        for j in range(table.columnCount):
            table.item(rowIndex, j).setBackground(color)

    def __init__(self, parent=None):
        super(WindowCommission, self).__init__(parent)
        self.parent = parent

        self.FrmPesquisaCommissionCreate()

        # conectando o click dos botoes
        self.btnBuscar.clicked.connect(self.PesquisarCliente)

        # criando um objeto cliente DB ara que fique a disposição para interagir com o módulo de acesso a dados

        # self.CenterOnScreen()

    def FrmPesquisaCommissionCreate(self):
        # =======================================================================
        # customizando a tela de pesquisa
        # =======================================================================
        self.setWindowTitle('Pesquisa de cliente')
        # self.resize(800, 300)

        self.layoutPrincipal = LayoutVertical()

        # criando o campo de busca
        self.lbcombo = Label('Selecione o filtro')
        self.combobox = ComboBox()
        self.combobox.addItem('Cliente')
        self.combobox.addItem('Fatura')
        self.combobox.addItem('Vencimento')
        self.combobox.addItem('Pagamento')
        self.combobox.addItem('Item')
        self.lblBusca = Label(self, 'Digite a busca')
        self.txtBusca = TextBox(self)
        self.btnBuscar = Button(self, '&Buscar')

        hboxBusca = LayoutHorizontal()
        hboxBusca.addWidget(self.lblBusca)
        hboxBusca.addWidget(self.txtBusca)
        hboxBusca.addWidget(self.btnBuscar)

        self.layoutPrincipal.addLayout(hboxBusca)

        # criando o grid da pesquisa
        atraso = []

        cabecalho_grid = [u'Cliente', 'Fatura', 'Emissão', 'Vencimento', 'Pagamento', 'Item', 'Valor Original',
                          'Valor Baixado', 'Comissão %', 'Comissão Valor', 'Imposto %', 'Imposto Valor',
                          'Comissão a pagar']
        self.columnCount = len(cabecalho_grid)
        self.grdPesquisaCliente = Table(self, count, len(cabecalho_grid))
        self.grdPesquisaCliente.setHorizontalHeaderLabels(cabecalho_grid)
        self.grdPesquisaCliente.resizeColumnsToContents()

        row = 0

        for cm in comissoes:

            if cm.vencto < cm.pagto:
                atraso.append(cm)
        for at in atraso:
            self.grdPesquisaCliente.setItem(row, 0, TableItem(at.client.name))
            self.grdPesquisaCliente.setItem(row, 1, TableItem(at.fatura))
            self.grdPesquisaCliente.setItem(row, 2, TableItem(str(at.emissao)))
            self.grdPesquisaCliente.setItem(row, 3, TableItem(str(at.vencto)))
            self.grdPesquisaCliente.setItem(row, 4, TableItem(str(at.pagto)))
            self.grdPesquisaCliente.setItem(row, 5, TableItem(at.item.item))
            self.grdPesquisaCliente.setItem(row, 6, TableItem(str(at.valor_orig)))
            self.grdPesquisaCliente.setItem(row, 7, TableItem(str(at.valor_baixado)))
            self.grdPesquisaCliente.setItem(row, 8, TableItem(str(at.comissao_percent)))
            self.grdPesquisaCliente.setItem(row, 9, TableItem(str(at.comissao_valor)))
            self.grdPesquisaCliente.setItem(row, 10, TableItem(str(at.imposto_percent)))
            self.grdPesquisaCliente.setItem(row, 11, TableItem(str(at.imposto_valor)))
            self.grdPesquisaCliente.setItem(row, 12, TableItem(str(at.comissao_pagar)))
            # else:
            #     self.grdPesquisaCliente.setItem(row, 0, TableItem(cm.client.name).setBackground(QColor(255,0,0)))
            #     self.grdPesquisaCliente.setItem(row, 1, TableItem(cm.fatura).setBackground(QColor(255,0,0)))
            #     self.grdPesquisaCliente.setItem(row, 2, TableItem(str(cm.emissao)).setBackground(QColor(255,0,0)))
            #     self.grdPesquisaCliente.setItem(row, 3, TableItem(str(cm.vencto)).setBackground(QColor(255,0,0)))
            #     self.grdPesquisaCliente.setItem(row, 4, TableItem(str(cm.pagto)).setBackground(QColor(255,0,0)))
            #     self.grdPesquisaCliente.setItem(row, 5, TableItem(cm.item).setBackground(QColor(255,0,0)))
            #     self.grdPesquisaCliente.setItem(row, 6, TableItem(str(cm.valor_orig)).setBackground(QColor(255,0,0)))
            #     self.grdPesquisaCliente.setItem(row, 7, TableItem(str(cm.valor_baixado)).setBackground(QColor(255,0,0)))
            #     self.grdPesquisaCliente.setItem(row, 8, TableItem(str(cm.comissao_percent)).setBackground(QColor(255,0,0)))
            #     self.grdPesquisaCliente.setItem(row, 9, TableItem(str(cm.comissao_valor)).setBackground(QColor(255,0,0)))
            #     self.grdPesquisaCliente.setItem(row, 10, TableItem(str(cm.imposto_percent)).setBackground(QColor(255,0,0)))
            #     self.grdPesquisaCliente.setItem(row, 11, TableItem(str(cm.imposto_valor)).setBackground(QColor(255,0,0)))
            #     self.grdPesquisaCliente.setItem(row, 12, TableItem(str(cm.comissao_pagar)).setBackground(QColor(255,0,0)))

            # if cm.vencto > cm.pagto:
            #     self.grdPesquisaCliente.setBackgroundRole(QBrush(Qt.red))
            row += 1
        self.grdPesquisaCliente.resizeColumnsToContents()
        self.showMaximized()
        self.layoutPrincipal.addWidget(self.grdPesquisaCliente)

        self.setLayout(self.layoutPrincipal)

    def PesquisarCliente(self, e):
        '''Método faz uma busca de clientes no banco de dados, e cria uma lista de tuplas'''
        # faz a consulta no banco e recebe uma lista de tuplas
        texto_busca = str(self.txtBusca.text())
        lista_dados_cliente = comission.get()

        # setando no grid a qtde de linhas, com a mesma qtde de registros da lista
        qtde_registros = len(lista_dados_cliente)
        self.grdPesquisaCliente.setRowCount(qtde_registros)

        linha = 0
        for cliente in lista_dados_cliente:
            # capturando os dados da tupla
            id_cliente = cliente[0]
            Nome_cliente = cliente[1]
            cidade_cliente = cliente[2]
            UF_cliente = cliente[3]

            # preencendo o grid de pesquisa
            self.grdPesquisaCliente.setItem(linha, 0, TableItem(id_cliente))
            self.grdPesquisaCliente.setItem(linha, 1, TableItem(Nome_cliente))
            self.grdPesquisaCliente.setItem(linha, 2, TableItem(cidade_cliente))
            self.grdPesquisaCliente.setItem(linha, 3, TableItem(UF_cliente))

            linha += 1

    def CenterOnScreen(self):
        '''alinhando a o formulario no centro da tela'''
        resolucao = QDesktopWidget().screenGeometry()
        self.move((resolucao.width() / 2) - (self.frameSize().width() / 2),
                  (resolucao.height() / 2) - (self.frameSize().height() / 2))


if __name__ == '__main__':
    import sys

    root = QApplication(sys.argv)
    app = WindowComissionMain()
    app.show()
    root.exec_()
